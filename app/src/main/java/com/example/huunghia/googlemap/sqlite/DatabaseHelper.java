package com.example.huunghia.googlemap.sqlite;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.example.huunghia.googlemap.table.TableMarker;

public class DatabaseHelper extends SQLiteOpenHelper {

    public static final String DB_NAME      = "marker.db";
    public static final int DB_VERSION      = 1;

    public DatabaseHelper(Context context) {
        super(context, DB_NAME, null, DB_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(TableMarker.SQL_CREATE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int i, int i1) {
        db.execSQL("DROP TABLE IF EXISTS " + TableMarker.TABLE_NAME);
        onCreate(db);
    }
}
