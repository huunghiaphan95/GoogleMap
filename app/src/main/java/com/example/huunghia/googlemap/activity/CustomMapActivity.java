package com.example.huunghia.googlemap.activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.huunghia.googlemap.R;
import com.example.huunghia.googlemap.models.MyMarker;
import com.example.huunghia.googlemap.table.TableMarker;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.ArrayList;
import java.util.HashMap;

public class CustomMapActivity extends AppCompatActivity implements OnMapReadyCallback {

    private GoogleMap mMap;
    private ArrayList<MyMarker> mMyMarkersArray = new ArrayList<MyMarker>();
    private HashMap<Marker, MyMarker> mMarkersHashMap;
    private TableMarker tableMarker;
    private ArrayList<MyMarker> items;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_custom_map);

        SupportMapFragment mapFragment =
                (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        tableMarker = new TableMarker(getApplicationContext());
        tableMarker.insertItem(new MyMarker("HHD Group", "icon1", Double.parseDouble("10.764691"),
                Double.parseDouble("106.694142")));
        tableMarker.insertItem(new MyMarker("Can nha A", "icon2",
                Double.parseDouble("10.765065"), Double.parseDouble("106.695075")));

        mMarkersHashMap = new HashMap<Marker, MyMarker>();

        /*mMyMarkersArray.add(new MyMarker("Brasil", "icon1", Double.parseDouble("10.764691"),
                Double.parseDouble("106.694142")));
        mMyMarkersArray.add(new MyMarker("United States", "icon2",
                Double.parseDouble("10.765065"), Double.parseDouble("106.695075")));*/

    }

    private void plotMarkers(ArrayList<MyMarker> markers) {

        if(markers.size() > 0) {
            for (MyMarker myMarker : markers) {
                // Create user marker with custom icon and other options
                MarkerOptions markerOption = new MarkerOptions()
                        .position(new LatLng(myMarker.getmLatitude(), myMarker.getmLongitude()));
                //markerOption.icon(BitmapDescriptorFactory.fromResource(R.drawable.currentlocation_icon));

                Marker currentMarker = mMap.addMarker(markerOption);
                mMarkersHashMap.put(currentMarker, myMarker);

                mMap.setInfoWindowAdapter(new MarkerInfoWindowAdapter());
            }
        }

        mMap.moveCamera(CameraUpdateFactory
                .newLatLngZoom(new LatLng(markers.get(0).getmLatitude(), markers.get(0).getmLongitude())
                        , 16f));
    }

    private int manageMarkerIcon(String markerIcon) {
        if (markerIcon.equals("icon1"))
            return R.drawable.icon1;
        else if(markerIcon.equals("icon2"))
            return R.drawable.icon2;
        else
            return R.drawable.icondefault;
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {

        mMap = googleMap;

        items = new ArrayList<>();
        items.addAll(tableMarker.listItems());

        plotMarkers(items);

        mMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {

            @Override
            public boolean onMarkerClick(com.google.android.gms.maps.model.Marker marker) {

                marker.showInfoWindow();
                return true;
            }
        });

    }

    public class MarkerInfoWindowAdapter implements GoogleMap.InfoWindowAdapter {


        public MarkerInfoWindowAdapter() {
        }

        @Override
        public View getInfoWindow(Marker marker) {
            return null;
        }

        @Override
        public View getInfoContents(Marker marker) {
            View v  = getLayoutInflater().inflate(R.layout.infowindow_layout, null);

            MyMarker myMarker = mMarkersHashMap.get(marker);

            ImageView markerIcon = (ImageView) v.findViewById(R.id.marker_icon);

            TextView markerLabel = (TextView)v.findViewById(R.id.marker_label);

            TextView anotherLabel = (TextView)v.findViewById(R.id.another_label);

            markerIcon.setImageResource(manageMarkerIcon(myMarker.getmIcon()));

            markerLabel.setText(myMarker.getmLabel());
            anotherLabel.setText("136 Cô Bắc, Cô Giang, Quận 1, Hồ Chí Minh");

            return v;
        }
    }

}
