package com.example.huunghia.googlemap.table;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.example.huunghia.googlemap.models.MyMarker;
import com.example.huunghia.googlemap.sqlite.DatabaseHelper;

import java.util.ArrayList;

public class TableMarker {

    public static final String TABLE_NAME       = "tbl_marker";
    public static final String COL_ID           = "id";
    public static final String COL_LABEL        = "label";
    public static final String COL_ICON         = "icon";
    public static final String COL_LATITUDE     = "latitude";
    public static final String COL_LONGTITUDE   = "longitude";
    public static final String SQL_CREATE       = "CREATE TABLE " + TableMarker.TABLE_NAME + " (" +
            TableMarker.COL_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
            TableMarker.COL_LABEL       +" TEXT, " +
            TableMarker.COL_ICON        +" TEXT, " +
            TableMarker.COL_LATITUDE    +" REAL, " +
            TableMarker.COL_LONGTITUDE  +" REAL)";

    private DatabaseHelper databaseHelper;

    public TableMarker(Context context) {
        databaseHelper   = new DatabaseHelper(context);
    }

    public boolean insertItem(MyMarker myMarker){
        SQLiteDatabase myDB     = databaseHelper.getReadableDatabase();

        ContentValues values    = new ContentValues();
        values.putNull(COL_ID);
        values.put(COL_LABEL, myMarker.getmLabel());
        values.put(COL_ICON, myMarker.getmIcon());
        values.put(COL_LATITUDE, myMarker.getmLatitude());
        values.put(COL_LONGTITUDE, myMarker.getmLongitude());

        long id = myDB.insert(TABLE_NAME, null, values);

        myDB.close();

        if(id == -1) return false;
        return true;
    }

    public ArrayList<MyMarker> listItems(){
        ArrayList<MyMarker> items = new ArrayList<MyMarker>();

        SQLiteDatabase myDB     = databaseHelper.getReadableDatabase();
        Cursor cursor = myDB.rawQuery("SELECT * FROM " + TABLE_NAME, null);
        /*Cursor cursor   = myDB.rawQuery("SELECT " + COL_LABEL + ", "
                                                    + COL_ICON + ", "
                                                    + COL_LATITUDE + ", "
                                                    + COL_LONGTITUDE + ", "
                                                    + COL_ID + " FROM " + TABLE_NAME, null);*/

        if(cursor.getCount() > 0) {
            cursor.moveToFirst();
            while (!cursor.isAfterLast()){
                String valueLabel      = cursor.getString(cursor.getColumnIndex(COL_LABEL));
                String valueIcon       = cursor.getString(cursor.getColumnIndex(COL_ICON));
                Double valueLatitude   = cursor.getDouble(cursor.getColumnIndex(COL_LATITUDE));
                Double valueLongitude  = cursor.getDouble(cursor.getColumnIndex(COL_LONGTITUDE));

                MyMarker myMarker = new MyMarker(valueLabel, valueIcon, valueLatitude,valueLongitude);

                items.add(myMarker);
                cursor.moveToNext();
            }
        }

        myDB.close();
        return items;
    }
}
